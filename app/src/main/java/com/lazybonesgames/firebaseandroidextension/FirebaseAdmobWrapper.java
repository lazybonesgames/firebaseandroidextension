//+package ${YYAndroidPackageName};
/*-*/package com.lazybonesgames.firebaseandroidextension;

//+import ${YYAndroidPackageName}.R;
//+import com.yoyogames.runner.RunnerJNILib;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.google.android.ump.ConsentForm;
import com.google.android.ump.ConsentInformation;
import com.google.android.ump.ConsentRequestParameters;
import com.google.android.ump.FormError;
import com.google.android.ump.UserMessagingPlatform;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ironsource.mediationsdk.IronSource;

public class FirebaseAdmobWrapper implements IExtensionBase {

    private final static int EVENT_OTHER_SOCIAL = 70;

    //region Analytics
    public static void logEvent(String eventName) {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(RunnerActivity.CurrentActivity);
        firebaseAnalytics.logEvent(eventName, null);
    }

    public static void logEventParamLong(String eventName, String paramName, double paramValue) {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(RunnerActivity.CurrentActivity);
        Bundle params = new Bundle(1);
        params.putDouble(paramName, Math.round(paramValue));
        firebaseAnalytics.logEvent(eventName, params);
    }

    public static void logEventParamString(String eventName, String paramName, String paramValue) {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(RunnerActivity.CurrentActivity);
        Bundle params = new Bundle(1);
        params.putString(paramName, paramValue);
        firebaseAnalytics.logEvent(eventName, params);
    }

    public static void logEventParamsStringInt(String eventName, String param1Name, String param1Value, String param2Name, double param2Value) {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(RunnerActivity.CurrentActivity);
        Bundle bundle = new Bundle(2);
        bundle.putString(param1Name, param1Value);
        bundle.putLong(param2Name, Math.round(param2Value));
        firebaseAnalytics.logEvent(eventName, bundle);
    }

    public static void logEventParamsStringString(String eventName, String param1Name, String param1Value, String param2Name, String param2Value) {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(RunnerActivity.CurrentActivity);
        Bundle bundle = new Bundle(2);
        bundle.putString(param1Name, param1Value);
        bundle.putString(param2Name, param2Value);
        firebaseAnalytics.logEvent(eventName, bundle);
    }

    public static void logEventParamsIntBoolean(String eventName, String param1Name, double param1Value, String param2Name, double param2Value) {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(RunnerActivity.CurrentActivity);
        Bundle params = new Bundle(2);
        params.putLong(param1Name, Math.round(param1Value));
        params.putLong(param2Name, Math.round(param2Value));
        firebaseAnalytics.logEvent(eventName, params);
    }

    public static void logEventParamsStringStringString(String eventName,
                                                        String param1Name, String param1Value,
                                                        String param2Name, String param2Value,
                                                        String param3Name, String param3Value) {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(RunnerActivity.CurrentActivity);
        Bundle bundle = new Bundle();
        bundle.putString(param1Name, param1Value);
        bundle.putString(param2Name, param2Value);
        bundle.putString(param3Name, param3Value);
        firebaseAnalytics.logEvent(eventName, bundle);
    }

    public static void logEventParams3StringLong(String eventName,
                                           String param1Name, String param1Value,
                                           String param2Name, String param2Value,
                                           String param3Name, String param3Value,
                                           String param4Name, double param4Value) {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(RunnerActivity.CurrentActivity);
        Bundle bundle = new Bundle();
        bundle.putString(param1Name, param1Value);
        bundle.putString(param2Name, param2Value);
        bundle.putString(param3Name, param3Value);
        bundle.putLong(param4Name, Math.round(param4Value));
        firebaseAnalytics.logEvent(eventName, bundle);
    }

    public static void logEventParams4StringLong(String eventName,
                                                 String param1Name, String param1Value,
                                                 String param2Name, String param2Value,
                                                 String param3Name, String param3Value,
                                                 String param4Name, String param4Value,
                                                 String param5Name, double param5Value) {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(RunnerActivity.CurrentActivity);
        Bundle bundle = new Bundle();
        bundle.putString(param1Name, param1Value);
        bundle.putString(param2Name, param2Value);
        bundle.putString(param3Name, param3Value);
        bundle.putString(param4Name, param4Value);
        bundle.putLong(param5Name, Math.round(param5Value));
        firebaseAnalytics.logEvent(eventName, bundle);
    }

    public static void logEventParamsStringIntIntFloatBoolean(String eventName,
                                                                String param1Name, String param1Value,
                                                                String param2Name, double param2Value,
                                                                String param3Name, double param3Value,
                                                                String param4Name, double param4Value,
                                                                String param5Name, double param5Value) {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(RunnerActivity.CurrentActivity);
        Bundle bundle = new Bundle();
        bundle.putString(param1Name, param1Value);
        bundle.putLong(param2Name, Math.round(param2Value));
        bundle.putLong(param3Name, Math.round(param3Value));
        bundle.putDouble(param4Name, param4Value);
        bundle.putLong(param5Name, Math.round(param5Value));
        firebaseAnalytics.logEvent(eventName, bundle);
    }

    public static void logLevelStart(String levelName) {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(RunnerActivity.CurrentActivity);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.LEVEL_NAME, levelName);
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LEVEL_START, bundle);
    }

    public static void logLevelEndParamsIntIntFloatBoolean(String levelName,
                                                           String param1Name, double param1Value,
                                                           String param2Name, double param2Value,
                                                           String param3Name, double param3Value,
                                                           String param4Name, double param4Value) {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(RunnerActivity.CurrentActivity);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.LEVEL_NAME, levelName);
        bundle.putLong(param1Name, Math.round(param1Value));
        bundle.putLong(param2Name, Math.round(param2Value));
        bundle.putDouble(param3Name, param3Value);
        bundle.putLong(param4Name, Math.round(param4Value));
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LEVEL_END, bundle);
    }

    public static void logEarnVirtualCurrency(String currencyName, double amount) {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(RunnerActivity.CurrentActivity);
        Bundle params = new Bundle(2);
        params.putString(FirebaseAnalytics.Param.VIRTUAL_CURRENCY_NAME, currencyName);
        params.putInt(FirebaseAnalytics.Param.VALUE, (int)Math.round(amount));
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.EARN_VIRTUAL_CURRENCY, params);
    }

    public static void logSpendVirtualCurrency(String currencyName, String itemName, double amount) {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(RunnerActivity.CurrentActivity);
        Bundle params = new Bundle(3);
        params.putString(FirebaseAnalytics.Param.ITEM_NAME, itemName);
        params.putString(FirebaseAnalytics.Param.VIRTUAL_CURRENCY_NAME, currencyName);
        params.putInt(FirebaseAnalytics.Param.VALUE, (int)Math.round(amount));
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SPEND_VIRTUAL_CURRENCY, params);
    }

    public static void setUserProperty(String name, String value) {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(RunnerActivity.CurrentActivity);
        firebaseAnalytics.setUserProperty(name, value);
    }
    //endregion

    //region Admob
    private final static int ASYNC_RESPONSE_ADMOB_INITIALIZED = 758900;
    private final static int ASYNC_RESPONSE_BANNER_SIZE = 758901;
    private final static int ASYNC_RESPONSE_REWARDED_ADS_EARN_REWARD = 758902;
    private final static int ASYNC_RESPONSE_REWARDED_ADS_DID_DISMISS = 758903;
    private final static int ASYNC_RESPONSE_INTERSTITIAL_ADS_DID_DISMISS = 758904;

    private final static int BANNER_TOP = 0;
    private final static int BANNER_BOTTOM = 1;

    private static RewardedAd mRewardedAd;
    private static AdView mAdView;
    private static InterstitialAd mInterstitialAd;
    private static int interstitialId = 0;

    private static boolean bannerSizeReported = false;

    public static void initializeAdmob() {
        ConsentInformation consentInformation = UserMessagingPlatform.getConsentInformation(RunnerActivity.CurrentActivity);

        ConsentRequestParameters params = new ConsentRequestParameters.Builder()
                .build();

        consentInformation.requestConsentInfoUpdate(RunnerActivity.CurrentActivity, params,
                () -> { //success
                    if (consentInformation.getConsentStatus() == ConsentInformation.ConsentStatus.REQUIRED && consentInformation.isConsentFormAvailable())
                        loadConsentForm();
                    else {
                        initMobileAds();
                    }
                },
                (FormError formError) -> {
                    initMobileAds(); //error
                }
        );
    }

    private static void loadConsentForm() {
        UserMessagingPlatform.loadConsentForm(RunnerActivity.CurrentActivity,
                (@NonNull ConsentForm consentForm) -> { //success
                    consentForm.show(RunnerActivity.CurrentActivity,
                            (@Nullable FormError formError) -> {
                                initMobileAds(); //OnConsentFormDismissedListener
                            }
                    );
                },
                (@NonNull FormError formError) -> initMobileAds() //error
        );
    }

    private static void initMobileAds() {
        MobileAds.initialize(RunnerActivity.CurrentActivity, (InitializationStatus initializationStatus) -> {
            int response = RunnerJNILib.jCreateDsMap(null, null, null);
            RunnerJNILib.DsMapAddDouble(response, "id", ASYNC_RESPONSE_ADMOB_INITIALIZED);
            RunnerJNILib.CreateAsynEventWithDSMap(response, EVENT_OTHER_SOCIAL);
        });
    }

    public static void initializeRewardedAds(final String adUnitId) {
        RunnerActivity.CurrentActivity.runOnUiThread(() ->
            RewardedAd.load(RunnerActivity.CurrentActivity, adUnitId, new AdRequest.Builder().build(), new RewardedAdLoadCallback() {
                @Override
                public void onAdLoaded(@NonNull RewardedAd rewardedAd) {
                    mRewardedAd = rewardedAd;
                    mRewardedAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                        @Override
                        public void onAdDismissedFullScreenContent() {
                            mRewardedAd = null;
                            initializeRewardedAds(adUnitId);

                            int response = RunnerJNILib.jCreateDsMap(null, null, null);
                            RunnerJNILib.DsMapAddDouble(response, "id", ASYNC_RESPONSE_REWARDED_ADS_DID_DISMISS);
                            RunnerJNILib.CreateAsynEventWithDSMap(response, EVENT_OTHER_SOCIAL);
                        }
                    });
                }

                @Override
                public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                    if (RunnerActivity.CurrentActivity != null) {
                        Log.i(RunnerActivity.CurrentActivity.getPackageName(), loadAdError.toString());
                        (new Handler(RunnerActivity.CurrentActivity.getMainLooper())).postDelayed(() -> initializeRewardedAds(adUnitId), 5000);
                    }
                }
            }));
    }

    public static double isRewardedAdsReady() {
        return mRewardedAd != null ? 1 : 0;
    }

    public static void showRewardedAds() {
        if (mRewardedAd == null)
            return;

        RunnerActivity.CurrentActivity.runOnUiThread(() -> {
            mRewardedAd.show(RunnerActivity.CurrentActivity, rewardItem -> {
                int response = RunnerJNILib.jCreateDsMap(null, null, null);
                RunnerJNILib.DsMapAddDouble(response, "id", ASYNC_RESPONSE_REWARDED_ADS_EARN_REWARD);
                RunnerJNILib.DsMapAddString(response, "type", rewardItem.getType());
                RunnerJNILib.DsMapAddDouble(response, "amount", rewardItem.getAmount());
                RunnerJNILib.CreateAsynEventWithDSMap(response, EVENT_OTHER_SOCIAL);
            });
        });
    }

    public static void initializeInterstitials(String adUnitId) {
        RunnerActivity.CurrentActivity.runOnUiThread(() ->
            InterstitialAd.load(RunnerActivity.CurrentActivity, adUnitId, new AdRequest.Builder().build(), new InterstitialAdLoadCallback() {
                @Override
                public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                    mInterstitialAd = interstitialAd;
                    mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                        @Override
                        public void onAdShowedFullScreenContent() {
                            mInterstitialAd = null;
                            initializeInterstitials(adUnitId);
                        }

                        @Override
                        public void onAdDismissedFullScreenContent() {
                            int response = RunnerJNILib.jCreateDsMap(null, null, null);
                            RunnerJNILib.DsMapAddDouble(response, "id", ASYNC_RESPONSE_INTERSTITIAL_ADS_DID_DISMISS);
                            RunnerJNILib.DsMapAddDouble(response, "interstitialId", interstitialId);
                            RunnerJNILib.CreateAsynEventWithDSMap(response, EVENT_OTHER_SOCIAL);
                        }
                    });
                }

                @Override
                public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                    if (RunnerActivity.CurrentActivity != null) {
                        Log.i(RunnerActivity.CurrentActivity.getPackageName(), loadAdError.toString());
                        (new Handler(RunnerActivity.CurrentActivity.getMainLooper())).postDelayed(() -> initializeInterstitials(adUnitId), 5000);
                    }
                }
            }));
    }

    public static double isInterstitialReady() {
        return mInterstitialAd != null ? 1 : 0;
    }

    public static double showInterstitial() {
        if (mInterstitialAd == null)
            return -1;
        else {
            RunnerActivity.CurrentActivity.runOnUiThread(() -> mInterstitialAd.show(RunnerActivity.CurrentActivity));
            return ++interstitialId;
        }
    }

    public static void showBanner(String adUnitID, double position) {
        if (mAdView != null)
            return;

        Display display = RunnerActivity.CurrentActivity.getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);
        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        mAdView = new AdView(RunnerActivity.CurrentActivity);
        mAdView.setAdUnitId(adUnitID);
        AdSize adSize = AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(RunnerActivity.CurrentActivity, (int) (widthPixels / density));
        mAdView.setAdSize(adSize);
        View rootView = RunnerActivity.CurrentActivity.findViewById(android.R.id.content).getRootView();
        if (rootView instanceof FrameLayout)
            mAdView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, position == BANNER_BOTTOM ? Gravity.BOTTOM : Gravity.TOP));
        else
            mAdView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            mAdView.setForegroundGravity(Gravity.CENTER);
        AdRequest adRequest = new AdRequest.Builder().build();

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                //sending banner size to GM
                if (!bannerSizeReported) {
                    int bannerSizeResponse = RunnerJNILib.jCreateDsMap(null, null, null);
                    RunnerJNILib.DsMapAddDouble(bannerSizeResponse, "id", ASYNC_RESPONSE_BANNER_SIZE);
                    RunnerJNILib.DsMapAddDouble(bannerSizeResponse, "banner_height", adSize.getHeightInPixels(RunnerActivity.CurrentActivity));
                    RunnerJNILib.CreateAsynEventWithDSMap(bannerSizeResponse, EVENT_OTHER_SOCIAL);
                    bannerSizeReported = true;
                }
            }
        });

        RunnerActivity.CurrentActivity.runOnUiThread(() -> {
            ((ViewGroup)rootView).addView(mAdView);
            mAdView.loadAd(adRequest);
        });
    }

    public static void removeBanner() {
        if (mAdView != null) {
            RunnerActivity.CurrentActivity.runOnUiThread(() -> {
                if (mAdView != null) {
                    mAdView.destroy();
                    ViewParent parent = mAdView.getParent();
                    if (parent instanceof ViewGroup)
                        ((ViewGroup) parent).removeView(mAdView);
                    mAdView = null;
                }
            });
        }
    }

    public static void setBannerVisible(double visible) {
        if (mAdView != null) {
            RunnerActivity.CurrentActivity.runOnUiThread(() -> {
                if (mAdView != null)
                    mAdView.setVisibility(visible == 1 ? View.VISIBLE : View.GONE);
            });
        }
    }

    @Override
    public void onResume() {
        IronSource.onResume(RunnerActivity.CurrentActivity);
    }

    @Override
    public void onPause() {
        IronSource.onPause(RunnerActivity.CurrentActivity);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {}
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {}
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {}
    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {return false;}
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {}
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {return false;}
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {return false;}
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {return false;}
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {return false;}
    @Override
    public Dialog onCreateDialog(int id) {return null;}
    @Override
    public boolean onTouchEvent(MotionEvent event) {return false;}
    @Override
    public boolean onGenericMotionEvent(MotionEvent event) {return false;}
    @Override
    public boolean dispatchGenericMotionEvent(MotionEvent event) {return false;}
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {return false;}
    @Override
    public boolean performClick() {return false;}
    @Override
    public void onNewIntent(Intent newIntent) {}
    @Override
    public void onStart() {}
    @Override
    public void onRestart() {}
    @Override
    public void onStop() {}
    @Override
    public void onDestroy() {}
    //endregion

}
